---
layout: post
title: WSL-2 With VPN
tags:
  - wsl
  - vpn
  - vpnkit
---

WSL-2 can lose the ability to connect to internet when the host is running a
VPN like GlobalProtect.

- The issue is described [here][wsl2p1].
- The steps for the work-around are [here][wsl2p2].
- The work-around substantially reduces the performance of OpenGL with the
  LLVM-pipe.

[wsl2p1]: https://github.com/microsoft/WSL/issues/5068#issuecomment-696199472
[wsl2p2]: https://github.com/sakai135/wsl-vpnkit

<!--more-->

## Details

- Some steps require WSL-2 to be connected to the internet.
- They might fail if the VPN be already active.
  - One work-around is to do all of the steps while connected to the native
    network, where the VPN might not be active.
  - Another work-around is to convert the Linux-distribution to WSL-1 when
    network-connectivity is needed and then to convert the distribution back to
    WSL-2 when done. (This can take a while if the distribution have a large
    file-system.)
  - I followed the procedure at my place of work after converting to WSL-2, and
    I had no problem; I have heard success from someone who converted between
    WSL-1 and WSL-2 to get it done.
- The steps for the work-around are [here][wsl2p2].  Some notes:
  - One might have to supply the argument `--no-check-certificate` when
    invoking `wget` if one's place of work have a man-in-the-middle system.
  - I had to reboot my machine after modifying `/etc/wsl.conf` to set
    `generateResolvConf=false`.
  - After coming back up, then I edited `/etc/resolv.conf`.
  - After the steps for setting it up are complete, the [wsl-vpnkit][wv] script
    must be run as root.
  - I installed a logical link to that script under
    `/home/tevaugha/Bin/wsl-vpnkit`.
  - I invoke it when I log in to Windows 10 by placing a shortcut in
    ```
    C:\Users\<UserName>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
    ```
    - The `Target`-field of that shortcut contains
      ```
      C:\Users\<UserName>\AppData\Local\wsltty\bin\mintty.exe --WSL="Debian" --configdir="C:\Users\<UserName>\AppData\Roaming\wsltty" bash -c "sudo /home/tevaugha/Bin/wsl-vpnkit"
      ```
      - I use `--WSL="Debian"` because one of the distributions that I have
        installed is called "Debian," but any distribution installed under
        WSL-2 would do.
      - This work-around allows all installed Linux-distributions to access the
        internet even though the script is run in only one of them.
        - Still, `/etc/wsl.conf` and `/etc/resolv.conf` need to be set up
          correctly in every distribution.
      - I happen to be using `wsltty` in this example.
        - Without `wsltty`, I might have made the `Target`-field instead be
          something much simpler:
          ```
          cmd.exe wsl bash -c "sudo /home/tevaugha/Bin/wsl-vpnkit"
          ```
    - In any event, when the window pops up after login, one has to type the
      password for one's account on the WSL-machine.
  - I have noticed that the work-around substantially reduces the performance
    of OpenGL with the LLVM-pipe.

[wv]: https://gitlab.com/tevaughan/config/-/blob/master/Bin/wsl-vpnkit

