---
layout: default
title: About
---

This site is Thomas E. Vaughan's latest attempt at a blog.

It is built via [Jekyll][1], and the theme is based on [YAMT][2].

[1]: https://jekyllrb.com
[2]: https://github.com/PandaSekh/Jekyll-YAMT.

