
# Source for [Thomas E. Vaughan's Current Blog, Scripta Varia][scripta]

[Scripta Varia][scripta] is a [Jekyll][jekyll]-based blog, and the theme is
based on [YAMT][yamt].

[scripta]: https://tevaughan.gitlab.io/scripta
[jekyll]: https://jekyllrb.org
[yamt]: https://github.com/PandaSekh/Jekyll-YAMT

