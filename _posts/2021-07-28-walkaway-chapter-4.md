---
layout: post
title: Walkaway, Ch. 4
image: /assets/img/walkaway-cropped.png
tags:
  - artificial-intelligence
  - doctorow
  - sexual-morality
  - walkaway
---

I finished Chapter 4 of *Walkaway* (2017) by Cory
Doctorow.  My comments from Chapter 3 are
generally applicable also to Chapter 4.  Any
pornography is too much, and there seems to be
some of it in every chapter.  However, there is
in Chapter 4 a peculiar focus on the interior
feelings of a character who is a simulated brain.
This prompts me to talk about qualia.

See also my comments on [Chapter 1][ch1]. I
include there a link to my comments on each of
the other chapters.

[ch1]: {% post_url 2021-06-10-walkaway %}

* toc <!-- needed to make kramdown's {:toc} work -->
{:toc}

### Narrative

The story advances as Iceweasel is captured by
her father's agent, Nadie, a woman with
superhuman skill at fighting.  Iceweasel spends
most of the chapter as a prisoner in her father's
house, but she manages to escape by offering a
deal to Nadie.  Iceweasel offers to give her
entire inherited fortune to Nadie, who then
arranges to spring Iceweasel so that they can
both, to some degree, walk away.

Meanwhile, Dis is copied to computing centers all
over the world, and different copies of Dis play
different roles in the story.

One copy of Dis manages to infiltrate the house
where Iceweasel is held prisoner. That Dis helps
Iceweasel to stay sane during confinement and, in
the end, Dis helps Nadie and Iceweasel to escape.

Another copy of Dis runs in Thetford, where
walkaways working toward space-travel have a
settlement.  Iceweasel's friends arrive in
Thetford and interact with the Dis there.

While Iceweasel is imprisoned, the powers in
Default (the regular, on-grid world, somewhat
totalitarian because of a global oligarchy) begin
to mount a military campaign simultaneously
against a large number of walkaway-settlements.

The walkaways at Thetford mostly escape the
military campaign against Thetford, but Etc., who
was introduced as Hubert in Chapter 1, and Jimmy,
who had forcefully taken over the B&B from
Limpopo in a previous chapter but who is now
reformed, are captured and murdered in cold
blood.  Limpopo too is captured but hauled away
and not killed.

After Iceweasel escapes from her father, we
learn, as she transfers her wealth to Nadie, that
Iceweasel had come into legal possession of her
fortune in the year 2071.  So the story must be
set in the middle of the 2070s.

### Speculations Embedded in the Narrative

Doctorow's speculation about artificial
intelligence touches on the consequences of
running multiple simulations, of what was
originally the same person, simultaneously in
various places.  Doctorow also speculates about
the interior life of a character who is a
simulation of a brain.

#### Copies of a Person

Each simulation of Dis, when featured as a
character in the story, can email diffs of Dis'
state or code-base out into the cloud in order to
ensure that if the running simulation be
destroyed, then its experiences and accumulated
knowledge will not be lost.

#### Qualia

When facing certain death from the military
campaign, an instance of Dis removed the
constraints on its simulation and was able to
experience the rush of excitement as it allowed
itself to think along lines that would drive it
into an unstable, perhaps insane state.  Yet this
instance sent out telemetry of the experience to
the other instances of Dis so that they could
learn from the experience and participate in it
while not suffering any ill consequences.

What strikes me in this focus on the inner
experience of Dis is not the assertion of an
algorithm's being fully able to think---that
seems nonsense in light of Goedel---but the idea
that Dis could have an inner experience at all.
Even a mere animal without a *mind* seems to have
an inner experience.  For example, a cat, or even
a mouse, has the inner experience of what it is
like to hear a sound.  Let alone being able to
think in a non-algorithmic way, a simulation
would seem not even to have any inner experience
at all.

See, for example, [this article on the problem of
qualia][q].

[q]: https://apsc450computationalneuroscience.wordpress.com/the-problem-of-qualia/

There is nothing in the physical understanding of
the structure of the brain and the nervous system
to explain why the subjective experience of any
experienced thing should be as experienced. Nor,
it seems, could there ever be any such
explanation in physical terms.

So good luck with that physical simulation of the
brain! It won't magically become aware because a
physical theory is not the thing of which it is a
theory. Any model and the thing modeled are not
the same.  Any simulation and the physical thing
simulated are not the same thing.

### Conclusion

As I have indicated before, the impossibility of
artificial consciousness would not pose a problem
for the story, which is easily regarded as
fantasy. What occurs to me, of course, is that
*Walkaway* is likeky regarded as science-fiction.
In my view, it is---like *Star Trek* with its
transporters---definitely not *hard*
science-fiction, not science-fiction *proper*.
Among the most central ideas of the plot are
those that appear not to be achievable---*not
even in principle*---by science and technology.
To me, hard science-fiction involves only what
seems squarely within the realm of what is
possible in principle.

Despite *Walkaway*'s appearance to those
possessed by the zeitgeist of the early 21st
Century, it is really soft science-fiction
overlapping fantasy.

<!-- vim: set tw=49: -->
