---
layout: post
title: Introduction to Programming 05
image: /assets/img/programming-cropped.png
tags:
  - find
  - ls
  - mv
  - programming
  - rm
  - shell
---

In this installment, for class on Sunday, 2021
Aug 01, we look a bit more at how to move things
around. Then we see how to clean up by removing a
file or even a whole directory, along with the
files and other directories that the directory
contains.

* toc
{:toc}

### Preparation

Before we begin, let us unpack an example-tree of
directories and files. I have prepared such a
tree ahead of time.  This tree has a top-level
directory called "`tree`." Under this are some
directories and files, each (whether file or
directory) with a random word for its name, and
each file has a random word for its content.

#### Downloading an Archive-File

To begin, click [this link][link] to download a
[compressed archive][archive].

[link]: /scripta/assets/src/test-tree.tar.gz
[archive]: https://en.wikipedia.org/wiki/Archive_file

#### Finding and Moving the File

Use the `mv`-command to *move* the downloaded
archive-file (`test-tree.tar.gz`)
- from wherever you downloaded it
- to your home-directory.

How to do this depends on what kind of system you
have.

- If you use a Linux-distribution as the primary
  operating system, then your browser likely
  downloaded '`test-tree.tar.gz`' into the
  directory named `Downloads` under your
  home-directory.  In that case, if the working
  directory be your home-directory, then you
  should be able to see the downloaded archive
  from your home-directory by doing

  ```
  $ cd
  $ ls Downloads
  ...  test-tree.tar.gz  ...
  $
  ```

  Use the `mv`-command to move it to the working
  directory:

  ```
  $ mv Downloads/test-tree.tar.gz .
  $
  ```

- If you use a Linux-distribution under Microsoft
  Windows via [WSL][wsl], and if you use a
  browser (like Edge) under Microsoft Windows to
  download the file, then it is likely located in
  the `Downloads`-directory in your account under
  Windows. You should be able to see the
  downloaded archive from your home-directory
  under Linux by doing

  ```
  $ cd
  $ ls /mnt/c/Users/<username>/Downloads
  ...  test-tree.tar.gz  ...
  $
  ```

  where `<username>` is the name of your account
  under Microsoft Windows. Use the `mv`-command
  to move it to the working directory:

  ```
  $ mv /mnt/c/Users/<username>/Downloads/test-tree.tar.gz .
  $
  ```

[wsl]: https://docs.microsoft.com/en-us/windows/wsl/about

#### Extracting the Archive

Finally, after making sure that the
home-directory is the working directory

```
$ pwd
/home/<username>
$
```

and making sure that `test-tree.tar.gz` is in the
working directory

```
$ ls test-tree.tar.gz
test-tree.tar.gz
$
```

use the [`tar`][tar]-command to unpack the archive:

```
$ tar fvx test-tree.tar.gz
test-tree/
test-tree/make-tree
test-tree/tree/
test-tree/tree/amidst/
test-tree/tree/amidst/codices
test-tree/tree/amidst/katydids
test-tree/tree/amidst/introduced/
test-tree/tree/amidst/introduced/sizzles/
test-tree/tree/amidst/introduced/sizzles/palavered
test-tree/tree/amidst/introduced/sizzles/podiatrist
test-tree/tree/amidst/introduced/sizzles/widescreen
test-tree/tree/amidst/introduced/sizzles/combats
test-tree/tree/amidst/introduced/conditioners/
test-tree/tree/amidst/introduced/conditioners/oatmeal
test-tree/tree/amidst/introduced/conditioners/fabricate
test-tree/tree/amidst/introduced/conditioners/grunt
test-tree/tree/amidst/introduced/conditioners/commenting
test-tree/tree/amidst/introduced/waterside
test-tree/tree/amidst/introduced/sideshow
test-tree/tree/amidst/ekes
test-tree/tree/crimsoning
test-tree/tree/foresee/
test-tree/tree/foresee/dumbwaiters
test-tree/tree/foresee/curtseying/
test-tree/tree/foresee/curtseying/nicks/
test-tree/tree/foresee/curtseying/nicks/equably
test-tree/tree/foresee/curtseying/nicks/handicrafts
test-tree/tree/foresee/curtseying/nicks/willows
test-tree/tree/foresee/curtseying/nicks/restaurant
test-tree/tree/foresee/curtseying/sketchier/
test-tree/tree/foresee/curtseying/sketchier/scratched
test-tree/tree/foresee/curtseying/sketchier/brutalities
test-tree/tree/foresee/curtseying/sketchier/commences
test-tree/tree/foresee/curtseying/sketchier/rush
test-tree/tree/foresee/curtseying/boots/
test-tree/tree/foresee/curtseying/boots/misruling
test-tree/tree/foresee/curtseying/boots/thrashings
test-tree/tree/foresee/curtseying/boots/endorsement
test-tree/tree/foresee/curtseying/boots/bandwagons
test-tree/tree/foresee/curtseying/bolero/
test-tree/tree/foresee/curtseying/bolero/norms
test-tree/tree/foresee/curtseying/bolero/hellions
test-tree/tree/foresee/curtseying/bolero/informed
test-tree/tree/foresee/curtseying/bolero/entered
test-tree/tree/foresee/fifths/
test-tree/tree/foresee/fifths/nicest/
test-tree/tree/foresee/fifths/nicest/toddles
test-tree/tree/foresee/fifths/nicest/bunk
test-tree/tree/foresee/fifths/nicest/typewriting
test-tree/tree/foresee/fifths/nicest/bend
test-tree/tree/foresee/fifths/whitewashed
test-tree/tree/foresee/fifths/dressiness/
test-tree/tree/foresee/fifths/dressiness/parliamentary
test-tree/tree/foresee/fifths/dressiness/milliner
test-tree/tree/foresee/fifths/dressiness/brusker
test-tree/tree/foresee/fifths/dressiness/recoiling
test-tree/tree/foresee/fifths/reappointment/
test-tree/tree/foresee/fifths/reappointment/gimmickry
test-tree/tree/foresee/fifths/reappointment/obligates
test-tree/tree/foresee/fifths/reappointment/speaker
test-tree/tree/foresee/fifths/reappointment/textiles
test-tree/tree/foresee/calculating
test-tree/tree/calve
$
```

[tar]: https://en.wikipedia.org/wiki/Tar_(computing)

Now you have a big tree of files to play with.
The tree itself is under the directory,
`test-tree/tree`, and the program that generated
it is a script named `test-tree/make-tree`.

### Moving a Directory

In the previous lesson, we learned about how to
move files around in the directory-structure.
Today, we shall take a brief look at how to move
a directory and everything underneath it.

Use the `cd`-command to make
`test-tree/tree/amidst` be the working directory.

```
$ cd test-tree/tree/amidst
$
```

Use the `ls`-command to see that there are three
files and one directory under the working
directory.

```
$ ls -F
codices  ekes  introduced/  katydids
```

Notice that by using the argument '`-F`' to the
`ls`-command, it prints the character '`/`' after
the node's name if the node be a directory. This
is useful especially if the terminal have no
ability to display color or if the `ls`-command
be not configured to give a special color to the
name of a directory.

Use the `mv`-command to move the
`introduced`-directory up one level.

```
$ mv introduced ..
$
```

Now do

```
$ cd
$
```

to change the working directory back to your
home-directory, and use the
[`find`][find]-command to print the tree that the
`tar`-command extracted from the archive.

```
$ find test-tree
test-tree
test-tree/make-tree
test-tree/tree
test-tree/tree/amidst
test-tree/tree/amidst/codices
test-tree/tree/amidst/katydids
test-tree/tree/amidst/ekes
test-tree/tree/crimsoning
test-tree/tree/foresee
test-tree/tree/foresee/dumbwaiters
test-tree/tree/foresee/curtseying
test-tree/tree/foresee/curtseying/nicks
test-tree/tree/foresee/curtseying/nicks/equably
test-tree/tree/foresee/curtseying/nicks/handicrafts
test-tree/tree/foresee/curtseying/nicks/willows
test-tree/tree/foresee/curtseying/nicks/restaurant
test-tree/tree/foresee/curtseying/sketchier
test-tree/tree/foresee/curtseying/sketchier/scratched
test-tree/tree/foresee/curtseying/sketchier/brutalities
test-tree/tree/foresee/curtseying/sketchier/commences
test-tree/tree/foresee/curtseying/sketchier/rush
test-tree/tree/foresee/curtseying/boots
test-tree/tree/foresee/curtseying/boots/misruling
test-tree/tree/foresee/curtseying/boots/thrashings
test-tree/tree/foresee/curtseying/boots/endorsement
test-tree/tree/foresee/curtseying/boots/bandwagons
test-tree/tree/foresee/curtseying/bolero
test-tree/tree/foresee/curtseying/bolero/norms
test-tree/tree/foresee/curtseying/bolero/hellions
test-tree/tree/foresee/curtseying/bolero/informed
test-tree/tree/foresee/curtseying/bolero/entered
test-tree/tree/foresee/fifths
test-tree/tree/foresee/fifths/nicest
test-tree/tree/foresee/fifths/nicest/toddles
test-tree/tree/foresee/fifths/nicest/bunk
test-tree/tree/foresee/fifths/nicest/typewriting
test-tree/tree/foresee/fifths/nicest/bend
test-tree/tree/foresee/fifths/whitewashed
test-tree/tree/foresee/fifths/dressiness
test-tree/tree/foresee/fifths/dressiness/parliamentary
test-tree/tree/foresee/fifths/dressiness/milliner
test-tree/tree/foresee/fifths/dressiness/brusker
test-tree/tree/foresee/fifths/dressiness/recoiling
test-tree/tree/foresee/fifths/reappointment
test-tree/tree/foresee/fifths/reappointment/gimmickry
test-tree/tree/foresee/fifths/reappointment/obligates
test-tree/tree/foresee/fifths/reappointment/speaker
test-tree/tree/foresee/fifths/reappointment/textiles
test-tree/tree/foresee/calculating
test-tree/tree/calve
test-tree/tree/introduced
test-tree/tree/introduced/sizzles
test-tree/tree/introduced/sizzles/palavered
test-tree/tree/introduced/sizzles/podiatrist
test-tree/tree/introduced/sizzles/widescreen
test-tree/tree/introduced/sizzles/combats
test-tree/tree/introduced/conditioners
test-tree/tree/introduced/conditioners/oatmeal
test-tree/tree/introduced/conditioners/fabricate
test-tree/tree/introduced/conditioners/grunt
test-tree/tree/introduced/conditioners/commenting
test-tree/tree/introduced/waterside
test-tree/tree/introduced/sideshow
$
```

[find]: https://en.wikipedia.org/wiki/Find_(Unix)

Compare tree printed by the `tar`-command when
you originally extracted the archive to the new
tree that you just printed by the `find`-command.
Do you see the difference?  You moved the subtree
rooted at the `introduced`-directory up so that
it is now right under the `tree`-directory and no
longer under the `amidst`-directory.

### Removing Things

Now let us clean up your home-directory.  The
[`rm`][rm]-command can be used to delete (remove)
a node, whether it be a file or a directory.

[rm]: https://en.wikipedia.org/wiki/Rm_(Unix)

#### Removing a File

To remove a file, one just types the path to the
file as the sole argument to the `rm`-command.
For example, try this:

```
$ rm test-tree/tree/introduced/sideshow
$
```

Run the `find`-command again as before in order
to see that the `sideshow`-file has been deleted.

Alternatively, use the `ls`-command to see that
the file has been deleted:
```
$ ls test-tree/tree/introduced
sizzles  conditioners  waterside
$
```

#### Removing Two or More Files Simultaneously

In order to remove two or more files at the same
time, just list each path as a different argument
on the command-line for the `rm`-command.

Try this:

```
$ cd test-tree/tree/foresee/fifths/reappointment
$ ls -F
gimmickry  obligates  speaker  textiles
$ rm gimmickry obligates textiles
$ ls -F
speaker
$ cd
$
```

We changed into the `reappointment`-directory
before using the `rm`-command because that
allowed us to type a short, relative path to each
file that we deleted. If we had not first changed
down into that directory, then we could still
have deleted the three files by listing them on
the command-line, but we should then have had to
list a much longer relative path from the
home-directory.

#### Removing a Directory

The `rm`-command can remove not only a file but
also an entire directory containing files and
other directories.

##### The Option-Argument '`-r`'

In order to remove a directory, give to the
`rm`-command the *option-argument* '`-r`' in
addition to the argument specifying the name of
the directory.

- The user has the *option* of using the
  `rm`-command to remove a directory, though the
  `rm`-command would by default (without '`-r`')
  produce an error if it were asked to remove a
  directory.

- The option-argument '`-r`' indicates that the
  `rm`-command should *recursively* remove the
  directory.  The specification of both '`-r`'
  and the path to a directory as argument to the
  `rm`-command means that not only will the
  directory be removed but also will all of its
  contents be removed; if any node under the
  directory be itself a directory, then all of
  *its* contents will be removed, too; etc.

##### The Option-Argument '`-v`'

Because of its recursive nature, removing a
directory usually results in the removal of more
than one node.  So it is often convenient, when
removing a directory, also to give to the
`rm`-command the argument '`-v`'.  The argument
'`-v`' indicates that the `rm`-command should be
*verbose*.  When the `rm`-command is verbose, it
prints the name of each node that it removes.  By
specifying the argument '`-v`', one can see how
many nodes are being removed. This is useful
information because one might not know the full
size of the tree rooted at the directory that is
being removed.

##### Combination of Single-Letter Options

Many a command in a unix-like system allows for
arguments to be combined.  This is true typically
for any command that accepts single-letter
options, each of which when specified on its own
is introduced by the character '`-`'.  For
example, the `rm`-command allows for the argument
'`-r`' and the argument '`-v`' to be combined on
the command-line as the single argument '`-rv`'
that specifies the effects of both the argument
'`-r`' and the argument '`-v`'.

For example, try this:

```
$ rm -rv test-tree/tree/foresee/curtseying/boots
removed 'test-tree/tree/foresee/curtseying/boots/misruling'
removed 'test-tree/tree/foresee/curtseying/boots/bandwagons'
removed 'test-tree/tree/foresee/curtseying/boots/thrashings'
removed 'test-tree/tree/foresee/curtseying/boots/endorsement'
removed directory 'test-tree/tree/foresee/curtseying/boots'
$
```

#### Removing Files and Directories Simultanously

The `rm`-command can remove not only
- multiple files in one go but also
- multiple nodes of any kind, file or directory.

The main restriction is that if at least one of
the nodes be a directory, then the user must also
supply the argument `-r`.  At least by
convention, any option-arguments that the user
would specify should appear first on the
command-line, before the list of paths to remove.
For example:

```
$ cd test-tree/tree/foresee
$ ls -F
calculating  curtseying/  dumbwaiters  fifths/
$ rm -rv calculating curtseying
removed 'calculating'
removed 'curtseying/nicks/equably'
removed 'curtseying/nicks/handicrafts'
removed 'curtseying/nicks/willows'
removed 'curtseying/nicks/restaurant'
removed directory 'curtseying/nicks'
removed 'curtseying/sketchier/commences'
removed 'curtseying/sketchier/rush'
removed 'curtseying/sketchier/brutalities'
removed 'curtseying/sketchier/scratched'
removed directory 'curtseying/sketchier'
removed 'curtseying/bolero/norms'
removed 'curtseying/bolero/entered'
removed 'curtseying/bolero/hellions'
removed 'curtseying/bolero/informed'
removed directory 'curtseying/bolero'
removed directory 'curtseying'
$
```

In this case, the `rm`-command removed both the
regular file `calculating` and the tree
consisting of directories and files under the
directory `curtseying`.

<!-- vim: set tw=49: -->
