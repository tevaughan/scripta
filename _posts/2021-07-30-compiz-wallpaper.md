---
layout: post
title: Compiz Wallpaper
image: /assets/img/compiz-cropped.png
tags:
  - compiz
  - wallpaper
  - gnome
  - ubuntu
---

I have a machine on which I run Ubuntu's compiz-9
on top of ubuntu-20.04.  I launch compiz
via a variant of the gnome-flashback session.  In
order to use compiz's wallpaper-plugin, I need a
couple of lines of special configuration.

```
$ gsettings set org.gnome.desktop.background draw-background false
$ gsettings set org.gnome.desktop.background show-desktop-icons false
```

<!-- vim: set tw=49: -->
