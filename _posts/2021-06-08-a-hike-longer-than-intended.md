---
layout: post
title: A Hike Longer than Intended
date: 2021-06-08 09:07:47 -0600
tags: [nature, artifice]
image: /assets/img/purple-flower-cropped.png
---

My hike this morning ended up taking me
not merely across roads near my hotel
but also over barbed-wire fences,
through tall grass, and through a couple of
miles of bush beside Interstate Highway 44.

One of my obstacles is pictured below.
I had to hop over the barbed-wire fence, to
get out onto the greenway
next to Interstate 44.

![purple flower against barbed wire]({{ "/assets/img/purple-flower.jpg" | relative_url }})

### I Should Have Known Better

On each of my first two mornings at the hotel
in Chickasha, I had walked in a loop. First,
I had walked south along US 277, which runs
in front of the hotel.  Then I had turned
right to head west along the first major
street that I had found. After perhaps half
a mile or so, I noticed that the road entered
onto a bridge a bit further on, but the bridge
seemed too narrow to admit pedestrians along
with cars. So I took the last right before the
bridge and eventually looped back to the hotel.

This morning, though, I wanted a longer walk.
After consulting Google Maps, I decided on a
route.

That route involved a different bridge,
and I should have guessed that it, too, might
be bad for the pedestrian.

### But I Set Out Anyway

The new route was roughly to be four miles
instead of two. About three miles in, though,
I came to the bridge. If it had been like the
one that I had seen on the first loop, then I
might have taken it, but this one was worse
because the traffic seemed greater.

Going back along the path that I had taken
seemed to be too long.

The bridge went over I-44.  There was no easy
way to get onto the lawn running along the
highway. But if I could do that, then I might
be able to get back in a mile or so, roughly
the same as crossing the bridge, though that
would require some luck in getting back away
from the highway.

I had to tresspass for a short distance
through private property.  There was a
driveway that led into what looked like a
trailer-park. The barbed wire ran along a
field of grass that seemed to be part of the
property. I found a place on the fence where I
could use a tree to help me over.

### An Environment Unfriendly to Pedestrians

The area around where I stayed in Chickasha is
definitely unfriendly to pedestrians.  There
are no sidewalks, not even right in front of
the hotel.  None.  To go for a walk means
either to compete for space on the street with
cars, to walk across part of a private lawn or
lot, or, when one be available, to walk along
an uneven easement.

It felt ridiculous, wrong, and inhuman.

This feeling only increased when I found myself
trying to deal with getting back to the hotel
without crossing a bridge that looked too
dangerous for a pedestrian to attempt with the
traffic at the time of morning when I was out.

After I managed to get onto the highway's
margin, there were two more problems.  First,
it was not always possible to walk where the
bush had been mowed.  In places, I had to walk
through grass and scrub perhaps five feet tall.
Second, I had to walk at least a mile beyond
the ideal exit-point because there was no way
off of the highway.  Not every street that
crossed the highway had access to the highway,
and the barbed-wire fencing, together with
untamed bush, made essentially impossible my
task of getting off.

After consulting Google Maps, I found that
the nearest good candidate for a place to
jump the fencing was at a park adjacent to
a mobile-home park.

In the end, my intended four-mile walk turned
into a six-mile hike involving some fairly
difficult fence-climbing and wading through
lots of thick, spider-web-laden bush, often
on a steep grade sloping precipitously down
toward the Interstate.

