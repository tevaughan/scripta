---
layout: post
title: Tawny Frogmouth Chick
date: 2021-07-06 08:34:27 -0600
image: /assets/img/tawny-frogmouth-chick-cropped.png
tags:
  - bing
  - chick
  - cute
  - frogmouth
  - tawny
  - wsl
---

Microsoft Bing put this picture up as my random
wallpaper of the day on my desktop. It is too
cute not to share.  Below is the full image.

[![tawny-frogmouth-chick][tfc]][tfc]

[tfc]: {{ "/assets/img/tawny-frogmouth-chick.jpg" | relative_url }}

Bing's control over my desktop-wallpaper does
imply that I am using Microsoft Windows as my
operating system.  My company-issued laptop has
to run Windows 10 as company-policy.

However, the [Windows Subsystem for Linux][wsl]
is now good enough so that I can do this and
still have most of my favorite unix-like apps at
hand and well integrated into the overall system.

[wsl]: https://docs.microsoft.com/en-us/windows/wsl/install-win10

<!-- vim: set tw=49: -->
