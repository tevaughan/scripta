---
layout: post
title: Exterior Calculus 1-1
tags:
  - edelen
  - exterior-calculus
  - math
image: /assets/img/applied-exterior-calculus-cropped.png
---

The first Chapter of _Applied Exterior Calculus_
by G. B. Edelen is entitled, "Mathematical
Preliminaries," and the first section is
entitled, "Number Space, Points, and
Coordinates."

### My Initial Trouble

Edelen begins with the introduction of $$E_n$$ as
"the $$n$$-fold Cartesian product of the real
line, $$\mathbb{R},$$ [so that $$E_n$$] may be
given a system of Cartesian coordinates, in which
case we say that $$E_n$$ is referred to a
Cartesian coordinate cover."

The problem is that, a few paragraphs later,
Edelen wants $$E_n$$ to be a set of geometric
points, each with no intrinsic coordinates, but
that conflicts with his definition here.

To see why, notice that the Cartesian product $$A
\times B$$ of two sets, $$A$$ and $$B,$$ is a set
of ordered pairs.

$$
A \times B =
  \{(a,b) | \; a \in A \; \text{and} \; b \in B\}
$$

For example, according to Edelen's definition
above,

$$E_2 = \mathbb{R} \times \mathbb{R} = \{(a,b) |
\; a \in \mathbb{R} \; \text{and} \; b \in
\mathbb{R}\}.$$

Generally, according to Edelen's definition,
$$E_n$$ is a set of Cartesian $$n$$-tuples, not
coordinate-system-independent, geometric points.

The choice of the symbol "$$E_n$$" seems to
invoke the idea of an $$n$$-dimensional
*Euclidean* space, which would just be a set of
points.  Only a Euclidean space (and not any
other space) admits a Cartesian
coordinate-system, but coordinates are extrinsic
and applied afterward to the points.

So it seems that Edelen's initial definition of
$$E_n$$ is just wrong, that $$E_n$$ is *not* "the
$$n$$-fold Cartesian product of the real line,"
and that $$E_n$$ is really better thought of,
according to its right name, as a Euclidean space
of geometric points, independent of any intrinsic
coordinate-system.

Let us instead designate "the $$n$$-fold
Cartesian product of the real line" as
$$\mathbb{R}_n$$.  *This* is the set whose every
element is a tuple of $$n$$ coordinates.

### Cover

Edelen is careful to insist that the coordinates
of the cover
- *neither* allow one to infer the distance
  between two points
- *nor* allow one to judge the angle at the
  intersection between two curves.

The *cover* seems to be a set of coordinates
associated with a homeomorphism $$c: E_n
\rightarrow \mathbb{R}_n,$$ such that, for any
point $$p \in E_n,$$ the coordinates $$q = c(p)$$
refer to the cover.

For $$c$$ to be a homeomorphism means that $$c$$
is a continuous isomorphism and thus has a
continuous inverse. Coordinates $$q$$ in the
cover do not in general determine distances and
angles in the domain of $$c.$$

### Edelen's Initial Maps

Despite Edelen's initial definition of $$E_n,$$
he seems to prefer conceiving of it as a set of
geometric points (rather than as a set of
Cartesian $$n$$-tuples according to the
definition).  In Edelen's apparent switch to this
new conception, an $$n$$-tuple appears in his
notation only by way of a set of $$n$$ maps,

\begin{eqnarray}
x^1 &:& E_n \rightarrow \mathbb{R}\\\\\\
x^2 &:& E_n \rightarrow \mathbb{R}\\\\\\
\vdots &&\\\\\\
x^n &:& E_n \rightarrow \mathbb{R}.\\\\\\
\end{eqnarray}

Collectively, these do the same job as my map
$$c$$ did above, but coordinate by coordinate,
and not by mapping each geometric point directly
to an $$n$$-tuple in the cover.  Rather, the
$$x^i$$---where $$i$$ is understood to pluralize
by ranging from 1 to $$n$$ inclusive---map from a
geometric point separately to each of the $$n$$
coordinates in the cover.

This seems not the right way to set up the
coordinates in the cover.  For specifying
topology, we might prefer a homeomorphism, but
Edelen's definitions seem not to make defining
such a thing easy.

Staying so close as possible to Edelen's
notation, I should say that we obtain coordinates
in the cover by way of a homeomorphism $$x: E_n
\rightarrow \mathbb{R}_n,$$ which is my $$c$$
above, but renamed.

The rest of the initial section tempts me to use
my notes, as an exercise, to translate Edelen's
notation into notation that I might prefer.

#### Mapping Points to Real Line

Consider an open subset $$U \subset E_n,$$ and
let $$p \in U$$.  Then $$x(p)$$ are the
coordinates of $$p$$ in the cover.  For mapping
$$E_n$$ to the real line, we write

$$
F: E_n \rightarrow \mathbb{R} \;|\;
f: \mathbb{R}_n \rightarrow \mathbb{R},
$$

where the auxiliary map $$f$$ following the
vertical bar gives a realization of $$F$$, but
from coordinates in the cover to the real line.
Then $$F(p) = f(x(p)).$$

#### Mapping Real Line to Points

Consider an open subset $$J \subset \mathbb{R}.$$
Then a curve in $$E_n$$ is specified by

$$
\Phi: J \subset \mathbb{R} \rightarrow E_n \;|\;
\phi: J \subset \mathbb{R} \rightarrow
\mathbb{R}_n,
$$

where the auxiliary map $$\phi$$ following the
vertical bar gives a realization of $$\Phi$$, but
from the real line to coordinates in the cover.
Let $$c \in J$$.  Then $$\phi(c)$$ are the
coordinates in the cover for a point on the
curve, and $$\Phi(c) = x^{-1}(\phi(c)).$$

#### Einstein's Notation

Edelen predicts that we shall need to use
Einstein's convention for summing over a repeated
index.  But it is not yet clear to me how I
should define indexed coordinates in terms of my
notion above, which is different from Edelen's. I
shall wait and see what needs to be done.

<!-- vim: set tw=49: -->
