---
layout: post
title: Delaunay and Voronoi
date: 2021-06-07 20:23:23 -0600
tags:
  - math
  - delaunay
  - voronoi
  - external-calculus
image: /assets/img/delaunay-voronoi-cropped.png
---

A text-message that Will Barham sent me when I was at one of Ana's
softball-games today reminded me of the Delaunay-triangulation and of its dual,
the Voronoi-diagram.

Will's message referred to the discretization of a PDE according to the
external calculus.  Discretization of the Hodge-star operator captures all of
the discretization-error. Ordinary derivatives depend only on the connectivity
of the mesh used for discretization, but the Hodge-star depends on the metric.
When Will mentioned that the best way to handle the problem is to use two
separate tessellations, "with the nodes of one at the barycenters of the
other," I was immediately reminded of the duality between
Delaunay-triangulation and the corresponding Voronoi-diagram.

So I looked up the [Delaunay-triangulation][1] to remind myself of what it is.
For a set $$ P $$ of points in the plane, a Delaunay-triangulation $$ D(P) $$
is such that for every triangle $$ T \in D(P), $$ no point $$ p \in P $$ lies
within the circle circumscribing $$ T $$.  That is, every $$ p \in P $$ lies
only *on* the circumference of one or more circumscribing circles but not
*within* any of them.

[1]: https://en.wikipedia.org/wiki/Delaunay_triangulation

The corresponding Voronoi-diagram is formed by connecting the center of each
circumscribing circle by a straight line to three of its neighboring centers,
so that each Voronoi-cell is bounded by a loop of such lines, and the
Voronoi-cell consists precisely of the points that are closest to the point $$
p \in P $$ contained within the cell.

