---
layout: post
title: Walkaway
date: 2021-06-10 07:46:01 -0600
tags:
  - artificial
  - doctorow
  - fiction
  - natural
  - property
  - responsibility
  - right
  - stallman
  - walkaway
  - rerum-novarum
  - leo-xiii
image: /assets/img/walkaway-cropped.png
---

I started reading *Walkaway* (2017) by Cory Doctorow.

The present post is only on the first chapter.  Comments on other chapters:

- [Chapter 2][ch2]
- [Chapter 3][ch3]
- [Chapter 4][ch4]

[ch2]: {% post_url 2021-06-11-walkaway-chapter-2 %}
[ch3]: {% post_url 2021-06-13-walkaway-chapter-3 %}
[ch4]: {% post_url 2021-07-28-walkaway-chapter-4 %}

In the first chapter, Doctorow seems to use his characters to comment on
corruption in the dystopian-future-setting's economic system and to justify
certain violations of the right to own property.

Doctorow's main character, Hubert, acknowledges the falseness of the attempt to
distinguish between human rights and property-rights because the right to
property *is* a human right.  Yet Hubert defends a coordinated effort
- to break into a factory, whose owner had shut down production and
- for the breakers in, while providing their own feed-stock, to use the factory
  to produce goods given away free of charge.

I am not sure how best to analyze this, but, first, we might distinguish---[as
I have seen Richard Stallman of the Free Software Foundation
do][slashdot]---between a *natural* right and an *artificial* right.  Second,
we might consider, according to *[Rerum Novarum][rerum]* of Pope Leo XIII, that
the owner of property has responsibilities to the common good.

[slashdot]: https://news.slashdot.org/story/00/05/01/1052216/thus-spake-stallman
[rerum]: https://www.vatican.va/content/leo-xiii/en/encyclicals/documents/hf_l-xiii_enc_15051891_rerum-novarum.html

### Natural Versus Artificial Rights

In 2000, Stallman on Slashdot [answered questions][slashdot] that were posed by
readers of Slashdot.

Stallman has some deep blindness and stupidity.  For example, he is an atheist,
and he provides no good reason for his atheism, even when given an obvious
prompt.

Nevertheless, Stallman also writes some interesting things worth reading.  For
example

> Q: Are there "natural" rights, and what is the nature of their existence?
>
> RMS: I think there are natural rights, natural in the sense that people are
> entitled to them regardless of what governments say about them. Freedom of
> speech is a good example; I think people are entitled to freedom of speech,
> and censorship is wrong. That is one example that I think most people
> reading this would agree with. I also believe that the freedom to share
> software and other published information is also a natural right.
>
> There are also artificial rights, rights that are not natural. I agree with
> the US legal system, for example, in the view that copyright is an
> artificial right, not a natural one. It can be reasonable to have a limited
> kind of copyright system for some kinds of works, but this is a concession
> made to benefit the public, not an entitlement of authors and publishers.
> This system should be limited so that it doesn't seriously conflict with
> other people's natural rights.

There is a natural right to property.  This right concerns a physical thing,
such as a bottle of beer, that one might have possession of.  If another person
take the bottle, then one loses it as the other gains it.

However, a human being might in some circumstances have no natural right to
control the copying of an encoding of some information.  Even if someone copy
information that I possess, then I do not lose my copy.

On Stallman's analysis (at least as I recall from reading, perhaps 30 years
ago, a treatment more expansive that what is quoted above), I have a natural
right to own and therefore to control *my* physical encoding of some
information (so that I have the natural right to refuse to allow anyone to make
a copy of my encoding), but if I have made a copy for myself and have given
that copy to another, then I do not have the *natural* right to prohibit the
other from allowing copies to be made.  The other does not deprive me of my
encoding by distributing copies of his copy of the encoding.

Yet copyright-law provides an *artificial* right for the originator of
information to control who may legally copy it, who may legally possess a copy,
etc.

In the example of the factory in Chapter 1 of *Walkaway*, on the present
analysis, the natural rights of the factory's owner were violated to some
degree:
- His factory (property) was broken into.
- His machinery (property) was used against his will.

But let's suppose that no harm was done, neither to his land, nor to his
buildings, nor to his machinery.

Then still his natural right to do what he would with his own property was
violated, though the violation was not grave, for neither was he deprived of
his property nor was his property used to do something intrinsically evil.

In the story, the legally serious violation was the violation of an artificial
right: The right to copy the design fed into the machine.  In the story, the
machine is programmable to make various things, and there was no legal license
to use a copy of the information that was loaded into the machine to make the
goods that were then given away free of charge.

### Reponsibility of the Owner of Property

Doctorow does not address this as I should like, but he does have his
characters consider the common good.  If idling the factory were considered as
a violation of the owner's responsibility to the public good, then that might
not justify even the light violation of the owner's natural property-rights,
but one could at least make a case for the owner's violation of responsibility,
on the basis of *[Rerum Novarum][rerum]* (in Paragraph 22):

> Private ownership, as we have seen, is the natural right of man, and to
> exercise that right, especially as members of society, is not only lawful,
> but absolutely necessary. "It is lawful," says St. Thomas Aquinas, "for a man
> to hold private property; and it is also necessary for the carrying on of
> human existence."  But if the question be asked: "How must one's possessions
> be used?" the Church replies without hesitation in the words of the same holy
> Doctor: "Man should not consider his material possessions as his own, but as
> common to all, so as to share them without hesitation when others are in
> need.  Whence the Apostle with, 'Command the rich of this world... to offer
> with no stint, to apportion largely.'"(12)  True, no one is commanded to
> distribute to others that which is required for his own needs and those of
> his household; nor even to give away what is reasonably required to keep up
> becomingly his condition in life, "for no one ought to live other than
> becomingly."(13)  But, when what necessity demands has been supplied, and
> one's standing fairly taken thought for, it becomes a duty to give to the
> indigent out of what remains over. "Of that which remaineth, give alms."(14)
> It is a duty, not of justice (save in extreme cases), but of Christian
> charity---a duty not enforced by human law.

<!-- vim: set tw=49: -->
