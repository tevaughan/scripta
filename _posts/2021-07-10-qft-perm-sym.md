---
layout: post
title: A QFT With Permutational Symmetry 1.1
date: 2021-07-10 06:05:34 -0600
image: /assets/img/qft-pm-cropped.png
tags:
  - holmes
  - particle-physics
  - physics
  - symmetry
---

I met Richard Holmes at the 8:00-AM Mass at St.
Francis of Assisi Catholic Church in Longmont. My
habit is to be an altar-server at 8:00 AM
whenever I can. I had already dressed in my
cassock and surplice and was standing with Father
Crisman before Mass. He seemed excited to
introduce me to Holmes, perhaps because Father
Crisman knew that I had earned a Ph.D. in physics
before I became a software-engineer.

### A Challenge

Chatting briefly with me before and then after
Mass, Holmes did not take long to claim that he
had come up with a new theory of matter. I did
not take long to realize that I was in the
presence of a man with a keen mind and the moral
discipline to focus it fruitfully.

I ordered Holmes' book, *A Quantum Field Theory
with Permutational Symmetry*, and the present
post is the first in what I hope to be a long
sequence on the book.

When I met Holmes for lunch, he challenged me to
review the book and to give him criticism.  I
have no reason to doubt that Holmes'
encouragement was genuine, but I recall feeling
that I am unlikely to come up with anything
useful after diving down into his text. I recall
telling him that if I did manage a review, then
it would take me months or, more likely, years.
I had only one semester of quantum field theory
in graduate school, and that was a lifetime ago.
More to the point, my not having been reading
papers in the relevant journals makes unlikely
not just my being able to provide anything useful
as a review but but even merely my being able to
comprehend the book at all.

So my effort here seems likely to be an exercise
in futility.

Yet I am hopeful of at least having some fun in
the attempt!

It's been a couple of months since I met with
Holmes and bought his book. I have several
projects going on outside of my paid job. But I
am now at least opening the book to look at the
first section of the first chapter.

### Why a New Theory

In the first section of the first chapter, Holmes
provides some motivation for his proposal.
Despite the amazing success of the standard model
of particle-physics, it has some clear
deficiencies.
- No explanation for why there are three
  generations of particle.
- No obvious reason for the ordering of the
  masses within each family.
- No explanation for "disparate particle states"
  in the matrices for the weak interaction.
- No explanation for free states in the
  lepton-family but bound states in the
  quark-family.
- No explanation for dark matter.

A nice, top-level overview of the standard model
can be [found here][standard].

[standard]: https://physics.info/standard/

I had forgotten the terminology. The *generation*
changes from *electron* to *muon* to *tauon* (or
its corresponding neutrino) in the charged and
uncharged particles of the lepton-family. The
generation changes from *up* to *charm* to *top*
(or its complement, *down*, *strange*, or
*bottom*, respectively) in the up- and
down-particles of the quark-family. A deficiency
in the standard model seems to be a lack of an
account for the number of generations.

The particle's mass generally increases from
generation to generation within the subfamily of
lepton or quark. Holmes seems to claim that the
standard model neither predicts this nor the
large difference from one subfamily to the next.

Never having worked with matrices for the weak
interaction, I am unfamiliar with what Holmes
means by disparate states, but I'll try to
remember that he counts some ad-hoc treatment of
the weak force as an incompleteness of the
standard model.

What I recall as "confinement" of quarks, each of
which exists only in a pair or a triplet (or
perhaps a larger grouping) is here referred to as
a requirement for bound states. The standard
model does not explain why quarks are always
bound, but leptons are not always bound.

Almost everybody has heard about dark matter, and
theories regarding it are various.
In graduate-school in the 1990s, I remember
reviewing the case for dark matter, which first
seemed needed in order to explain the apparent
rate at which a star orbits the center of a
disk-galaxy. In a typical disk-galaxy, even a
star near the edge of the visible disk orbits
with a speed almost equal to that of a star
orbiting close to the galaxy's nucleus.  But
there is no observational evidence for the matter
that would need to be present to explain a
relatively high orbital velocity for stars far
from the nucleus. Eventually, there was evidence
showing the need for dark matter from the
dynamics of galaxies and, ultimately, from the
fluctuations in the cosmic microwave background.
So the final deficiency that Holmes lists is the
standard model's not predicting a particle that
could serve as the dark matter observed by
astronomers and observational cosmologists.

Yet Holmes proposes to redress all of these
shortcomings and more.

<!-- vim: set tw=49: -->
