---
layout: post
title: Walkaway, Ch. 3
date: 2021-06-13 16:30:04 -0600
tags:
  - artificial-intelligence
  - doctorow
  - fiction
  - sexual-morality
  - walkaway
  - penrose
image: /assets/img/walkaway-cropped.png
---

I finished Chapter 3 of *Walkaway* (2017) by Cory
Doctorow.  See also my comments on [Chapter
1][ch1]. I include there a link to my comments on
each of the other chapters.

[ch1]: {% post_url 2021-06-10-walkaway %}

Despite the unfortunate aspects of Chapter 2,
the characters have been intelligent and
interesting all along. Also, apart from its
pornographic aspects, the narrative is
engaging.

* toc <!-- needed to make kramdown's {:toc} work -->
{:toc}

Chapter 3 focuses on Natalie, introduced in
Chapter 1 but now known as "Iceweasel." The
pornography first displayed in Chapter 2 is here
neither so prolonged nor quite so graphic as it
was there. Yet the treatment of sex is overall no
better. The introduction of the lesbian, Gretyl,
and the biologically male transsexual, Tam,
highlight Doctorow's unrealistic portrayal of his
female characters in general.

There is also the unfortunate treatment of
artificial intelligence, but this is not a severe
impediment to appreciation of the story. Almost
every science-fiction story has some impossible
magic, disbelief in which the reader must
suspend. The new character, Dis, is a
neuroscientist who had had her brain scanned
before she was killed. In Chapter 3 we first meet
Dis when the simulation of her brain is "booted"
and then rebooted over and over, each time after
a modification intended to keep her from
committing mental suicide when she panics over
not really being herself. In the world of the
story, Dis ends up being the first successfully
running simulation of a conscious human brain.

### Homosexuality and Transsexuality

In both Doctorow's specific treatment of lesbian
behavior and the general idea, that a biological
male might be mentally female, there seems to be
a falseness.

Doctorow views and writes about sex from an
obviously male point of view. His treatment of
female sexuality reflects---as it seems to
me---more of what a man would naturally fantasize
or hope for than of what is real.  None of the
serious constraints particular to women and their
sexual behavior are mentioned.  For example,
there is no mention of the monthly cycle and of
the effect that different phases of it have on
sexual desire.

The appearance of Tam highlights the general
tendency of men to fantasize about or to pretend
what a woman is. I recall reading an account of
why Paul McHugh, when head of psychiatry at Johns
Hopkins, eventually shut down, in 1979, the
experimental program of sex-reassignment surgery.
I cannot now find the article, but I think that
it might have been in Scientific American in the
late 1990s.  McHugh looked at multiple lines of
evidence.  Along one line, men who underwent
surgery and received sex-hormones were no less
likely to commit suicide than those who did
neither.  Along another line, a female
psychiatrist had conversations with each male
candidate for reassignment. The result of this
was that the woman was overwhelmingly likely to
evaluate the man as definitely thinking like a
man, especially about matters related to sex. The
male candidate seemed not so much to be a woman
trapped in man's body as to present himself as
what a man *imagines* a woman to be.

[This article][m] by McHugh is not the one that I
should prefer to cite, but it is the best that I
have so far found. It eliminates details of the
evaluation by the female psychiatrist and
summarizes that result as "gals know gals."

[m]: https://www.firstthings.com/article/2004/11/surgical-sex

The character of Tam, according to his
description in the text, did not undergo the
surgery, but he does seem to be taking female
hormones.

Anyway, both Doctorow's treatment of the attitude
of women toward sex and the fact that he has a
transsexual character remind me of the male
tendency to view a woman as he might prefer her
to be rather than as what she really is.

### Artificial "Intelligence"

In light of certain facts of history (especially
in mathematics and philosophy), there is also a
falseness in Doctorow's assertion, that the
simulation of a brain could be a mind.  Yet it
seems to me both fun and useful to write stories
exploring the nature of an artificial
intelligence, as if it could exist.

One case for the impossibility of a machine's
being a mind is given by Roger Penrose in his
*Shadows of the Mind* (1994).  Penrose requires
several chapters to make this case, but the short
of it is that, at least on certain occasions---as
when inventing a certain kind of proof for a
theorem---the human mind behaves
*non*-algorithmically.  This is important
because, so far as it functions according to its
design, a machine can only ever function
*algorithmically*.  As part of the justification
for his claim, Penrose invokes Goedel, who showed
that there is no algorithm that can prove all
possible theorems from a set of axioms.  For any
algorithm that can prove theorems from a set of
axioms, Goedel was able to show that there is
always a true theorem that the algorithm cannot
prove.  That a mathematical mind can always prove
theorems unprovable by any algorithm shows,
according to Penrose, that a mathematical mind
cannot be a machine (because a machine acts
according to an algorithm).

In another case, there is Thomas Aquinas' point,
that each of what he terms "the three acts of the
mind,"

- *apprehension* of a *universal* in the meaning
  of a word,
- *judgment* of the meaning of a sentence, and
- *reasoning* to the construction of a
  deductively valid syllogism,

is an action that matter cannot even in principle
perform.  Diving into what Aquinas means by
"apprehension," "judgment," and "reason" would
take us too far afield in this post.  I note for
the moment only that what Aquinas means by
"reason" is not the process that an algorithm
would go through to prove a theorem from axioms.
Perhaps I shall revisit the acts of the mind in a
future post.

There are other cases besides.

There are good reasons according to which man can
know that artificial intelligence of the kind
depicted by Doctorow is impossible.  Yet a story
featuring a character like Dis can be a good one.
I love the character of Commander Data in *Star
Trek: The Next Generation*.  Exploring such a
character can sometimes give insight into human
nature.

### Conclusion

Both thumbs down on Doctorow's treatment of human
sexuality.  When glorified by an author in his
protagonist, the lie that a man tells himself
about women only cheapens and mars what might
otherwise be a good story.

Although artificial intelligence of the kind
depicted by Doctorow is impossible, it can be fun
and interesting to consider in the context of
fiction.  But this moves the story toward the
genre of fantasy and away from what I should
consider proper science-fiction.

<!-- vim: set tw=49: -->
