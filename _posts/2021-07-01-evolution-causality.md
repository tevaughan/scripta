---
layout: post
title: Evolution and Secondary Causality
date: 2021-07-01 08:59:28 -0600
image: /assets/img/aquinas-cropped.png
tags:
  - atheism
  - causality
  - dodds
  - evolution
  - feser
  - instrumental-causality
  - intelligent-design
  - rosenberg
  - secondary-causality
---

I saw a bumper sticker whose words ran something
like this: "If a gun kill a person, then a pencil
misspells a word."

That made me think.

Although I did not reach a conclusion about
gun-control, the bumper sticker prompted me to
think about causality, how one thing, a cause,
can lead to another thing, the effect.

* toc <!-- needed to make kramdown's {:toc} work -->
{:toc}

(This present article is essentially a copy of
what I posted back in 2014 to my 'blogspot.com'
site.  Apparently, my account is no longer active
there.  As I write this, [the old post][esc] is
still accessible on the internet, but my name is
no longer associated with it!)

[esc]: https://baconeggsandbeer.blogspot.com/2014/07/evolution-and-secondary-causality.html

### Instrumental Causality

In *Unlocking Divine Action*, Dominican Father
Michael Dodds refers to chalk as a writing
instrument:
> Sometimes a creature, even while exercising its
> own proper causality, achieves something beyond
> its natural capacity. Chalk, for instance, by
> its own nature, may be able to leave marks when
> it is drawn across a surface. If it [leave]
> intelligible marks in the form of letters and
> words, however, this effect must be due not
> just to the chalk but to some intelligent agent
> employing the chalk as an instrument. The chalk
> is an "instrumental cause," and the one who
> uses it is a "principal cause."

The instrumental cause operates in one way, and
the principal cause operates in a deeper way. Let
us consider two cases, in each of which a pair of
causes brings about the same effect.

1. In the first case, consider two instrumental
   causes of the same effect. These causes
   interfere. For example, each of two men
   carrying a table is a cause of the table's
   motion. If one man carry more of the table's
   weight, then the other carries less of the
   table's weight. Some of the table's motion is
   due to one man, and the rest is due to the
   other. One man's causal influence interferes
   with the other's because the more one man
   causes the table to move, the less the other
   man causes the table to move.

2. In the second case, consider a principal cause
   and an instrumental cause of the same effect.
   These causes do not interfere. For example,
   each of the chalk and the person who wields it
   is entirely the cause of a writing. The
   writing is not divided up as though part of
   the writing is caused by the chalk and another
   part is caused by the wielder. Every bit of
   the writing is caused by the chalk, and every
   bit of the writing is caused by the wielder.
   The chalk's causal influence does not
   interfere with that of the wielder because the
   wielder's causal influence is what allows the
   chalk to have its influence.

According to the doctrine of instrumental
causality, a principal cause and an instrumental
cause work together in different ways and do not
interfere.

### Secondary Causality

A special case---or perhaps the prototype---of
instrumental causality is secondary causality.
Aquinas calls every created thing a "secondary
cause," and God is the "primary cause." A piece
of chalk causes a mark on a chalkboard. A gust of
wind causes leaves to rustle. The primary cause,
however, is what gives every created thing the
power to cause something else. The effect of the
primary cause is for the secondary cause to have
its particular effect.  As the principal cause
and the instrumental cause do not interfere, so
the primary cause and the secondary cause do not
interfere. Just as each of the chalk and its
wielder is entirely the cause of the written
word, so too each of the gust and the One Who
gives every gust the power to rustle leaves is
entirely the cause of the rustling of the leaves.
Instrumental causality and secondary causality
involve deeper and shallower causes that do not
interfere; one cause, operating at a deeper level
than the other, enables the other cause to have
its effect.

### Randomness as a Creature

When God is considered as the primary cause,
God's action in the world, unlike the actions of
created things, is of a completely different
category. A logically consistent theory of
evolution does not, for the generation of new
species, force a choice between God's
intervention on the one hand and random variation
on the other. In a Catholic view of evolution,
God is the cause of the randomness that brings
about new species. In other words, the Catholic
can admit that randomness is real and really is
random. God knows what will result from it, but
God's knowledge, transcendent of secondary
causes, does not deprive randomness of its nature
relative to other secondary causes. The
randomness is just another created thing, another
secondary cause.

### Feser on Secondary Causality

Edward Feser, a Catholic philosopher, has been a
popular exponent of Aristotelian and Thomistic
thought in the first couple of decades of the
21st Century.  In the following quotation, Feser
is responding to Alex Rosenberg, author of The
Atheist's Guide to Reality. [Feser writes][rr6]:
> Rosenberg’s entire argument rests on a crude
> misunderstanding of the nature of divine
> causality. In particular, he evidently knows
> nothing about the traditional distinction
> between primary causality and secondary
> causality, and operates with a crudely
> anthropomorphic conception of deity. For he
> assumes that making evolution compatible with
> theism would require supposing that God
> intervenes in biological history at various
> points in order to alter the course of events
> so as to ensure that homo sapiens ... comes
> about, but ... in so subtle a way that it
> [only] looks like the product of random
> variation and natural selection....
>
> But this ... is like saying that the author of
> a science fiction novel in which ... a species
> comes about via natural selection has to
> “intervene” at key points so as to make sure
> that the evolutionary process comes out the way
> he needs it to ... for the story to work -- but
> at the same time has to do so subtly, so that
> none of the characters would guess that he had
> intervened in this way. The very suggestion is
> silly, for the author isn’t one causal factor
> in the story among others. His causality
> relative to the story is not at all like the
> causality of either the characters or the
> impersonal processes operating within the
> story.
>
> Similarly, on the classical [theist's]
> conception of God, God is not one causal factor
> in the universe among others, not even an
> especially grand and powerful causal factor. He
> is not a “first” cause in the sense of being
> followed in a temporal series by a second
> cause, a third cause, a fourth cause, etc.
> Rather, He is “first” or primary in the sense
> of being the fundamental cause, the necessary
> precondition of there being any causality
> within the universe at all, just as the author
> of a story is the “first cause” of what happens
> in the story, not in the sense of generating
> effects in the way the characters and processes
> described in the story do, but rather in the
> sense of being the necessary precondition of
> there being any characters or processes in the
> story at all. Things in the world are
> “secondary” causes, then, in the sense of
> deriving their being and causal power from God,
> just as the characters in the story have any
> reality and causality at all only because the
> author of the story has imparted it to them by
> virtue of writing the story....
>
> Now, it would be absurd to suggest that Macbeth
> did not really murder Duncan, but that it was
> Shakespeare who committed the murder and merely
> made it look like Macbeth had done it. This
> would be to treat the author as if he were a
> character in the story. For the same reason, it
> would be absurd to suggest that in a science
> fiction novel in which ... a species evolves,
> it is not really Darwinian processes that
> generate the species, but rather the author of
> the story who does so and merely made it seem
> as if Darwinian processes had done it. But by
> the same token, it is absurd to suggest that if
> God [create] a world in which human [bodies]
> come about by natural selection, He would have
> to intervene in order to make the Darwinian
> processes come out the way He wants them to, in
> which case they would not be truly Darwinian.
> This is to confuse primary with secondary
> causality, to think of God as if He were merely
> one causal factor in the world among others,
> like treating an author as if he were merely
> one character in his story among the others....

[rr6]: http://edwardfeser.blogspot.com/2012/01/reading-rosenberg-part-vi.html

### Against "Intelligent Design"

Feser goes on to point out that the advocate of
Intelligent Design makes the same mistake as the
atheist. Each confuses primary and secondary
causality. However, there is an interesting sense
in which the atheist is closer to the Catholic
than is the proponent of Intelligent Design:
Neither the atheist nor the Catholic sees the
need to invoke God as acting in the same way as
secondary causes, the ones that can be studied by
way of modern science.

<!-- vim: set tw=49: -->
