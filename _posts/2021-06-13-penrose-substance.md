---
layout: post
title: Irreducibility, Emergence, and Substance
tags:
  - accident
  - artificial-intelligence
  - causality
  - emergence
  - feser
  - intelligent-design
  - irreducibility
  - penrose
  - reductionism
  - secondary-causality
  - substance
image: /assets/img/aristotle-cropped.png
---

Roger Penrose's claim of the non-algorithmic
nature of the mind is like the claim of the
irreducibility of an Aristotelian substance to an
arrangement of its ingredients. In
[*Shadows of the Mind*][sm] and in Penrose's
[response to criticism][rcs] of *Shadows*,
Penrose argues that a mind could never be a
machine and that the laws of nature as presently
understood do not allow for a brain to function as
a mind does. In [Aristotle's Revenge][ar], Edward
Feser gives
an introduction to the idea of an Aristotelian
substance, which, if macroscopic, is not
reducible to atoms or molecules; they would not
*actually* exist in a macroscopic substance. I
propose a simulation-based criterion for
reducibility, and this criterion expresses the
Aristotelian idea in terms similar to those used
by Penrose. Penrose's claim might be seen as a
special case of a general kind of physical
irreducibility.

[sm]: https://en.wikipedia.org/wiki/Shadows_of_the_Mind
[rcs]: https://www.calculemus.org/MathUniversalis/NS/10/01penrose.html
[ar]: https://thomistica.net/book-reviews/2019/12/12/what-is-the-philosophy-of-nature-review-of-fesers-aristotles-revenge

* toc <!-- needed to make kramdown's {:toc} work -->
{:toc}

(Feser has mentioned that *laws of
nature* do not necessarily provide the best
framework for modern science. He has pointed to
Nancy Cartwright as an example of a modern
philosopher who entertains this idea. She has written
rather about causal powers inherent in things, with
"law of nature" merely an imperfect summary of how things
with intrinsic causal powers behave. This seems
relevant to my development below.)

### Reducibility

Sometimes the whole is just the sum of its parts,
or, rather, sometimes the whole is *reducible* to
its parts. What I mean by "reducible" is as
follows:

If whole $$W$$ be reducible to an arrangement
$$A(P)$$ of the elements in the set $$P$$ of
$$W$$'s parts, then
- $$W$$ is nothing but $$A(P)$$;
- the parts in $$P$$ actually exist in $$W$$; and
- $$W$$ is the application of an accidental
  form $$A$$ to the secondary matter represented
  by $$P$$.

The meaning for each of "actually," "accidental
form," and "secondary matter" is discussed in the
section [Substance](#substance) below.

#### Simulation as Test of Reducibility

For the present discussion, which concerns
physical things, I propose a test of
reducibility: that a *computer-simulation* of
the behavior of $$W$$ in terms of $$A$$ and $$P$$
must be able to predict whatever is observed
through experimental interaction with $$W$$.

#### The Usual Suspects

For the reduction of biology or chemistry to some
more fundamental theory, the parts in every
biological or chemical whole would be molecules,
atoms, or subatomic particles, depending on the
case.

For the reduction of mind, the parts would be
neurons or anything more fundamental.

### Irreducibility

Some philosophers of chemistry have argued
that quantum theory does not account for
everything in chemistry. (For example, the electronic
configuration of the atoms.) It seems that
simulation by way of quantum theory either
- would fail to predict features of a system
  regardless of the investment of
  computer-time or
- fails only because the simulation is limited
  by a maximum
  practical computational complexity.

If it were only an issue of computational
complexity, so that an impractically long run of
simulation would be required to account for what
is observed, then would that be enough to make
some philosophers of chemistry, since the early
1990s, claim that chemistry is not reducible to
physics?

I am not sure.

(See Pages 25 and 27 in Joachim Schummer's
Chapter 2 of [*Philosophy of Chemistry: Synthesis
of a New Discipline*. Boston Studies in the
Philosophy of Science, Vol. 242.  Dordrecht,
Springer, 2006, pp 19-39.][snd]

[snd]: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.405.629&rep=rep1&type=pdf

See also Section 6 (Chemical Reduction) of the
[article (of 2019) on the philosophy of chemistry
in the Stanford Encyclopedia of Philosophy.][cr])

[cr]: https://plato.stanford.edu/entries/chemistry/#CheRed

Yet even if the problem were one of
complexity in computation, then that, on its own,
might be enough to justify the claim that there
is a real irreducibility going on. For example,
if the number of steps needed for a proper
simulation of a system, in terms of a fundamental
theory, were greater than the number of particles
in the observable universe, then the reduction of
the system to that theory would seem both hard to
test and impractical to make use of.

The possible irreducibility of chemistry to
particle-physics, and, what seems definite from
the Aristotelian point of view, the
irreducibility of *biology* to particle-physics,
remind me of Penrose's claim of the
irreducibility of mind to any simulation (that
is, to any algorithm). From my point of
view---needing more study of the philosophy of
chemistry---Penrose's claim seems relatively
strong: Penrose makes the case for irreducibility
in principle, no matter how fast the computer
might be, no matter how much computing time might
be given. Further, he argues that mind cannot be
reduced to any physical laws of nature at all,
not as we currently understand laws of nature.

#### Against "Intelligent Design"

Writing about irreducibility reminds me of
["intelligent design"][id] or "ID." I oppose ID
so far as I oppose inferring, from any evidence of
complex design, God’s special, non-revelatory
intervention in history. One does not need
complexity in the world to argue for God as the
cause of the world (for example, Aquinas does not
make any of his Five Ways rest on complexity). Also,
the existence of things that seem too complex, when viewed
through the lens of a reductionist ontology, is hardly a
reason to require that God serve merely as a
convenient filler of a gap in man's understanding,
as an intervener like Zeus invoked to explain
apparent lightning in a mechanical account of evolution.

[id]: https://www.newworldencyclopedia.org/entry/Intelligent_design

Of course God creates
everything at all times, eternally, ex nihilo, but
this creation of history is not an intervention in
history; the primary cause does not *compete* with
secondary causes, just as the hand that holds the
pencil does not compete with the pencil for being
the cause of what is written onto the page when the
pencil is drawn across the page. Each of the pencil
and the holder of the pencil is fully the cause of
what is written, but each in a different sense.

Although God has miraculously intervened in man's
recorded history for the purpose of revelation,
miraculous action has traditionally been regarded
by Christians as *extraordinary*.
God's special intervention to inject into ordinary,
natural things a design that is not built into His
creation of nature ex nihilo would seem to lower
some of the divine action to the level of a
creature, a merely secondary cause.

[ID seems wrong][sc]
so far as it stands against the doctrine of
secondary causality. The theory of ID seems to
depend on reductionism that is avoided by way of
admitting substances other than fundamental
particles and laws of nature. ID also seems to
depend on an idea of randomness inconsistent with
the doctrine of secondary causality.

[sc]: {% post_url 2021-07-01-evolution-causality %}

There is an alternative to special, non-miraculous
intervention: God creates nature from eternity so
that one thing can naturally arise from others,
even as the one is not *reducible* to the others;
at the very least, not reducible in terms of man's
theories of nature. This is an alternative to
the conception of a pagan god who serves the
role of a great secondary cause by injecting
divine irreducibility into preexisting nature as
needed.

#### For Natural Irreducibility

There
seems to be the *natural* irreducibility of a
physical thing to the material ingredients that
combined to form it.

For example, if some of the observable properties
of an iron bar cannot be predicted on the basis
of the standard model of particle-physics, then
one would seem to have encountered an example of
natural irreducibility. So, too, even on the basis
of a theory of the iron atom.
One might be tempted to
say that an iron bar is unimaginably and
irreducibly complex when viewed in terms of
fundamental particles, or even iron atoms, but the
vast complexity might
be an artifact of the theory's referral,
erroneous in the context of the bar,
to actual particles in the bar. If it be erroneous
to regard the bar as made of actual particles,
then the
underlying theory would not properly describe the
bar (at least not all of its behavior) even if the
computational power for simulation
were unlimited.

One might prefer the view in which the bar is
made of a macroscopic *substance*, in which iron
atoms exist only *potentially* or *virtually*.
After all, one can tell
directly that the bar actually exists, but nobody
will ever directly sense an iron atom, neither
by vision, by hearing, by taste, by smell, nor by
touch.

Yet despite the insensibility of an iron atom,
and even if it do *not* exist actually in an
iron bar, one might still suppose that the iron atom
*does* exist actually in a context whose theoretical
description *does* allow proper simulation of the
system in terms of the atom.

### Substance

The appearance of natural irreducibility suggests
the Aristotelian notion of *substantial change*
as well as the notion of a macroscopic substance.

On the Aristotelian view, matter of ordinary
experience is *secondary matter*. It can be
arranged according to an *accidental form*, such
as the shape of a piece of iron or the
three-dimensional arrangement of different parts
of a machine. An object of ordinary experience is
thus a union of form and matter.  This is the
basic idea of *hylemorphism* (literally
"wood-form" but conceptually matter-form) in
metaphysics.  Matter is the principle of
potentiality; it can take on any of a wide range
of forms.  Form is the principle of actuality; an
actual object has a particular form.

However, the union of form and matter goes
deeper. When considering an object of a single
substance, such as a volume of pure, liquid
water, one may understand the substance itself as
the union of a *substantial form* (what it is to
be liquid water) with *primary matter* (what
could be any substance).  That union results in
the substance that can then take on an accidental
form.

- Primary matter has the potential to be any
  substance, to take on any substantial form.

- Secondary matter, composed of one or more
  substances, has the potential to be any shape,
  temperatue, etc., consistent with its
  composition, to take on any accidental form.

Feser points out that the Aristotelian notion of a
substance might be taken to apply to the
particles of the standard model of
particle-physics, or perhaps to atoms as the case
may be.  There may be a context in which a
fundamental particle, like an electron, is a
substance and another context in which an atom
is a substance, irreducible to electrons and
quarks. This seems hardly controversial
because such an approach is implicitly taken by
some folks who mistake modern science for
metaphysics.

#### Fundamental, Physical Actuality

But Feser himself argues that a *living
individual plant or animal* is a substance!

And there's the rub.

The idea that a living creature is not *actually*
made of atoms is foreign to the modern mind.
Similarly, the idea that liquid water might not
*actually* be made of atoms is foreign to the
modern mind.  Yet, on the Aristotelian view, a
substance is a *fundamental* actuality in the
physical world.

##### A Ground for Potentialities and Accidents

In one way, a substance is fundamental in its
providing a ground in which potentialities and
accidental forms can be rooted. For an example of
potentiality, if each of liquid water and ice be
a substance, then the potential to be ice is a
real potential in liquid water, and the potential
to be liquid water is a real potential in ice.
For an example of an accident, if solid bronze be
a substance, then the shape of the surface of a
lump of bronze is an accident, which (as the
formal cause) combines with the substance (as the
material cause) to yield a physical object.

##### Not an Arrangement of Other Actualities

In a second way, a substance is fundamental in
that it is not actually composed of parts that
are more fundamental.

- The body of a mechanical watch is not a
  substance because the parts are not combined
  into a radical unity by any principle in the
  watch. If a gear be carefully taken out of the
  watch, that removal would do nothing to the
  gear's ability to remain exactly what it is.
  The gear does not depend for its existence on
  anything in the wider body. In this sense, the
  gear is more fundamental than the watch.

- However, the body of a living tree is a
  substance.  If a leaf be cut from a living
  tree, then the leaf will cease to be what it
  was, an integral part of the unity of a single
  living substance. In this sense, the leaf is
  not more fundamental than the living
  individual, though the leaf is a part. The leaf
  depends for its existence on the rest of the
  tree's body.

This second way of seeing that a substance is
fundamental in material reality is equivalent to
recognizing that a substance is the uniting of a
substantial form with primary matter.

- The body of the mechanical watch is not a
  substance because the form of the matter in
  each of the watch's *parts* (such as the gear)
  is a substantial form (informing primary matter),
  but the form of the *watch as a whole* is
  accidental. The accidental form does not inform
  primary matter but merely informs the arrangement
  of the parts, each a piece of secondary matter.

- On the other hand, the form of a living tree is
  a substantial form that informs primary matter
  directly and in a way that brings a radical
  unity and fundamental interdependence among the
  tree's parts. When the leaf is cut from the
  tree, the leaf begins to die, which means that
  the form of the leaf becomes merely an
  accidental form that arranges various bits of
  secondary matter, now tending toward decay.

#### A Ground for Potentialities

One difference between a potentiality and an
actuality is that a potentiality is real only *in
something else* that is actual, whereas an
actuality is real *in itself*.

- The potentiality to become liquid is real only
  in something else like ice, but an actual piece
  of ice is real in itself.

- A *virtual* water-molecule exists only in
  something else like a living organism, but an
  actual water-molecule exists in itself.

Feser points out that "virtuality" signifies the
kind of potentiality that a particle or molecule
might have when incorporated into a substance.

The option of holding that liquid water might be
a substance, meaning that water-molecules exist
only virtually---not actually---in liquid water,
is, in some sense, *enabled* by the absolute
unobservability of a water-molecule to human
sensation.

One might distinguish between

- a substance (like an individual water molecule)
  that could never, not even in principle, be
  observed directly by the senses and

- one (like a chunk of bronze) that could.

Because one could not sense a water-molecule in
liquid water even if it were actually present,
one may, in the absence of convincing arguments
one way or the other, suppose either that the
molecules are actually present or that they are
not.

The hylemorphic metaphysics seems not completely
against reductionism.  The relevant principle
here is the first of the Thomistic theses:

*Potentia et actus ita dividunt ens, ut quidquid
est,*
- *vel sit actus purus,*
- *vel ex potentia et actu tamquam primis atque
  intrinsecis principiis necessario coalescat.*

(See the [article on the 24 theses][24th] with
contributions from Lumbreras, McDonald, Hugon, et
al.)

[24th]: http://www.u.arizona.edu/~aversa/scholastic/24Thomisticpart2.htm

Potency and act so divide being, as that whatever
is,
- either would be Pure Actuality
- or of potentiality and actuality as primary and
  intrinsic principles would necessarily be
  composed.

(This is my own translation into English, and so
it is probably not the best.)

To deny that the ingredients exist *actually* in
the new substance is not to deny that they exist
*virtually* in the substance. A real potentiality
(including virtuality) in a substance is a
reality. The substance might have the potential,
under the right circumstances, to yield back the
ingredients that formed it. One might affirm that
the ingredients exist as virtualities in the new
substance, even if they do not exist as
actualities in the new substance.

#### A Ground for Change

A key idea related to actuality and potentiality
is that a potentiality can become an actuality
(and an actuality can become a potentiality).  In
fact, this is the summary of how all change
happens on the Aristotelian view.

Being a ground for potentialities that can inhere
within it, a substance is then a ground for
change.

- One substance might have within itself the
  potential to become another substance (as when
  ice melts into liquid water, if they be
  substances).

- A substance might have the potential to become
  a virtual part of another substance (perhaps as
  when a molecule of water in vapor condenses
  into a pool of liquid water).

- If a substance have a virtual part, then
  something else actual, when interacting with
  the substance, could actualize the part
  (perhaps as when applying heat to liquid water
  causes the evaporation of an actual
  water-molecule).

- If each of two substances have virtual parts,
  then the interaction of the substances could
  cause a part of each substance to become
  actualized and to interact (perhaps as when
  firing an X-ray beam into a sample of liquid
  water causes the scattering of an actualized
  X-ray photon off of an actualized electron).

Each of the above corresponds to a kind of
substantial change.

#### Irreducibility in Principle

From an Aristotelian ontological perspective,
what would keep a substance (like the macroscopic
iron in an iron bar) from being reducible to its
ingredients (like iron atoms) is the presence of
a substantial form (like the substantial form of
macroscopic iron), which reaches all the way down
to inform primary matter directly and is not an
accidental, microscopic arrangement of the
ingredients (like iron atoms), each as a
secondary material cause.  From the perspective
of modern physics, this same idea could be
reflected as the practically infinite complexity
in the attempt to represent the iron bar merely
as an arrangement of atoms and/or the sheer
inability in principle for a proper simulation in
terms of atoms to exist.

One might suspect irreducible complexity in any
attempt to explain a substance merely as an
accidental arrangement of an immense number of
things that combined to make it.  At least part
of what it means to be a substance is that it is
*not* an accidental, microscopic arrangement of
substantial ingredients; the ingredients cease to
exist *actually*, cease to be substantial, but
might continue to exist *virtually* in the new
substance.

#### Reductionism as Potential

If an object $$O$$ made of a single substance
$$S$$ be potentially reducible to a collection of
distinct parts,

  - $$P_1$$ of substance $$S_1,$$
  - $$P_2$$ of substance $$S_2,$$
  - etc.,

then the reduction of $$O$$ to $$P_1, P_2,$$
etc., is a potentiality that inheres in $$S$$ and
thus too in $$O$$.  (Note that $$S_1$$ might be
$$S$$ or a substance other than $$S$$; also,
$$S_1$$ and $$S_2$$ might be either the same or
different substances; etc.)

$$P_1, P_2,$$ etc., exist virtually (not
actually) in $$O$$. For example, if a
water-molecule be a substance, then, when it is
incorporated into the substance $$S$$ of a living
individual animal or plant $$O$$, the
water-molecule loses its substantiality but
retains its reality as it becomes a virtual
$$P_\text{W}$$ in the individual. Any object
$$O$$ of a single substance $$S$$ is fundamental
in the sense that $$O$$ is not actually composed
of distinct parts $$P_1, P_2,$$ etc., though
$$O$$ could potentially be broken down into
$$P_1, P_2,$$ etc., which would become actual as
the break-down occurs.

- For example, when a living individual dies,
  some virtual substances in its living body
  become actual in distinct objects (such as
  decaying muscle, decaying fat, etc.) in the
  corpse.

- For another example, suppose that bronze is a
  substance. An object of bronze can be cut into
  two pieces. Each of those new pieces of bronze
  existed virtually in the initial object but
  became an actual piece of bronze only after the
  cut occurred.

- For yet another example, suppose that a
  water-molecule in a thin gas is an object of a
  single substance. Further, suppose that an
  OH-molecule and an H-atom exist as virtual
  parts of the water-molecule. When struck by a
  photon of the appropriate wavelength, the
  water-molecule is broken into the OH, which is
  an object of a single substance, and the H,
  another object of single substance. The
  OH-molecule is named precisely after its parts
  that exist virtually, not actually, within it.
  Similarly, if the H-atom be a substance, then
  the electron and proton exist only virtually
  within it, not as actualities.

### Irreducibility of Mind to Algorithm

Now that we have introduced some fundamental
considerations related to irreducibility, from
metaphysics and from the idea of a substance, let
us review Penrose's case for irreducibility of
mind to algorithm.  Penrose does not invoke any
of the ideas related to hylemorphism, but he does
consider the idea of a simulation, which is what
I introduced above as the mechanism for testing
the idea of reducibility.

A simulation of a brain will involve the
conceptual breaking down of the brain into parts
of some kind, perhaps neurons with certain
properties and arrangement. In the simulation, an
algorithm updates the state of each part in a way
that is consistent across every part in the
simulation. Although Penrose argues in very
general terms, without mentioning any such
details of simulation, he is talking about the
reduction of mind to a set of physical parts, the
parts that are considered in the development of a
simulation.

#### Four Views of Consciousness

In *Shadows*, Penrose identifies four mutually
exclusive points of view on consciousness.

1. "All thinking is computation; in particular,
   feelings of conscious awareness are evoked
   merely by the carrying out of appropriate
   computations."

2. "Awareness is a feature of the brain's
   physical action; and whereas any physical
   action can be simulated computationally,
   computational simulation cannot by itself
   evoke awareness."

3. "Appropriate physical action of the brain
   evokes awareness, but the physical action
   cannot even be properly simulated
   computationally."

4. "Awareness cannot be explained by physical,
   computational, or any other scientific terms."

Penrose identifies Number 3 as his own.

Traveling down the list of views, one finds that
Number 3 is the first view violating [my
criterion for reducibility](#test). Although
framed in terms of computation, Penrose's own
view is an assertion of the irreducibility of the
mind to any set of physical laws that, like all
physical laws since Newton, have so far been
simulatable.  His path toward solution is to look
for radically different, non-algorithmic laws of
nature. It seems to me, though, that he might
better look along the line in which a "law" of
nature (whether it be quantum field theory,
general relativity, or a successor) is viewed as
always an approximate summary of what's going on
by virtue of interactions among things according
to their own, innate causal powers (active and
passive potentialities, to use classical
terminology).

#### Simulation and Mind

Penrose's view distinguishes between
- physical action that *can* be simulated and
- physical action that can *not* be simulated.

In his view, at least *some* physical action in
the world cannot be simulated.  That is, the
occasional thing in the world is not reducible to
a system obeying (algorithmic) laws of nature.
The specific claim is that such action happens at
least in the *conscious brain*.

At least three claims ground Penrose's view:
- First, that every system behaving according to
  a current physical theory behaves in a way that
  can be described algorithmically; that every
  current physical theory is simulatable.
- Second, that a mind sometimes acts as no
  algorithm could; that a formal system according
  to Gödel corresponds to an algorithm.
- Third, that consciousness is entirely physical
  despite its non-algorithmic action; that the
  human mind has a completely materialistic
  basis.

Penrose is thus driven to conclude that we need a
physical theory of a type never before proposed.
Penrose himself does not propose such a theory,
but he argues that it would need to be such that
a system described by it would behave, in at
least some circumstances, non-algorithmically.

So far as Penrose identifies and presents the
facts, I find him convincing.  I happen to
disagree with his opinion on materialism, but his
overall presentation feels to me insightful and
humble.

### Simulation Generally

I should like to be precise---more precise than
Penrose is in *Shadows*---about what a simulation
is.  A simulation (in my experience) typically
breaks a physical system into conceptual parts,
each of which behaves according to a simple set
of rules for interacting with its neighboring
parts.  An algorithm then applies the rules to
each part in order to derive a consistent state
for the system as a whole.  For example, a model
of a liquid might break down the volume of the
liquid into a number of volume-elements, and the
course of the volume's evolution might be broken
into steps in time.  If the pieces be small
enough in space and/or time, then one can
simplify the rules applied to each piece while
still making an accurate prediction of the
overall behavior of the system.

#### How to Represent a System in Simulation

The question is, What are the natural, conceptual
pieces that allow the system to be properly
simulated?

If it turn out, for example, that there is indeed
a theory allowing proper simulation of a liquid's
physical action, it seems to me at least unlikely
(and perhaps impossible) that such a properly
predictive simulation would be had by breaking
the liquid into electrons and quarks as the parts
of the simulation.  That is, for a proper
simulation to exist, the physical theory might
need to be a theory (like one involving the
Navier-Stokes Equations) that is *not* the most
fundamental physical theory presently in
existence (like a quantum field theory for the
standard model of particle physics).

The most fundamental physical theory of matter
might *not* enable the proper simulation of any
material system familiar to common human
experience, not even the proper simulation of a
system (like a volume of liquid) substantially
less complex than a living brain. It would seem
interesting to investigate whether the issue
merely be one of computational complexity, or
whether there simply be no algorithm according to
which the behavior of a liquid could follow from
a quantum field theory of fundamental particles.

Just as no physical theory (not even a theory so
ad-hoc and far-removed from fundamental physics
as desired) can properly, according to Penrose,
simulate a mind, so, too, perhaps, no fundamental
physical theory can properly simulate every system
of common experience.

This idea seems unlikely to have been treated by
Penrose when he wrote *Shadows* because the
recognition that chemistry (and *a fortiori* the
fluid-dynamics of a liquid) is not reducible to
fundamental physics seems not to have begun
gaining serious traction among philosophers of
chemistry until the early 1990s, around the time
when Penrose wrote *Shadows*. (See the reference
above to Schummer's review.)

If even a relatively small system of common
experience cannot be simulated by way of too
fundamental a physical theory, then this would
provide an example of irreducibility distinct
from that of Penrose. Even if it did turn out
that a liquid of common experience could in
principle be properly simulated on the basis of
quantum field theory (though this seems extremely
unlikely to me), it seems certain to me that a
living individual plant or animal could not be
simulated in terms of quantum field theory.  This
is all quite apart from any consideration of the
simulation of a conscious mind.

#### Consequence of Inability to Simulate

It seems to me that

- if a theory allow proper simulation of a system
  of separate particles (like atoms of hydrogen
  and oxygen), but

- if the theory do not allow proper simulation of
  a system in which the particles combine to form
  an apparently new thing with its own rules
  (like a living plant),

then we might profit from wrestling with the idea
of a substance.

The particles might exist as separate substances
in one context (perhaps as substantial atoms of
hydrogen and oxygen in a gas), but they seem in
another context (that of a living organism) no
longer simply to exist as they were.  Rather, in
the latter context, there seems to be a new
substance *instead* of the particles that
combined to form it. Yet the particles that
formed the new substance seem to exist in *some
sense* within the new substance.  After all, one
seems able to provide an appropriate stimulus to
the new substance in order to bring the particles
back to substantiality. For example, one can by
electrolysis transform liquid water into hydrogen
and oxygen.

### Conclusion

I recall first encountering in the 1980s the idea
of *emergence* and that of an *emergent property*
of a complex system.  But it seems to me that
there are two basic kinds of emergence.

#### Two Kinds of Emergence

In the first place, there is the system that is
composed of a large number of actual parts and
has properties that can be understood by careful
examination of how those parts interact.
Consider, for example, a pile of gravel. Under
the acceleration of gravity at the surface of the
Earth, a pile of gravel will have a natural angle
for the slope of its side relative to the
horizontal.  This angle naturally emerges as a
conveyor-belt deposits ever more gravel onto the
center of the pile.  The angle, I suspect, can be
properly predicted by a simulation in which the
pile is treated as individual pebbles of gravel.
If so, then that would be consistent with the
actuality of the pebbles in the pile.

In the second place there is the substance (such
as a living plant, or perhaps even a crystal or a
pool of water) that radically incorporates
molecules (virtual or actual) as it grows, so
that a molecules that was actual loses its
actuality on being incorporated but nevertheless
retains its reality as a virtual molecule.
Irreducibility of the substance would mean that
its behavior cannot properly be simulated in
terms of molecules. Such inability to simulate
the substance on the basis of its ingredients is
what one would *expect* if the substantial form
directly order the primary matter that had been
ordered by the substantial form of each molecule
before it was incorporated.

- For the substantial form to order the primary
  matter according to the substance's own nature
  means that the substance can have
  potentialities other than what would be due to
  the molecular ingredients.

- For the substance to have among its
  potentialities that of producing again the
  actual molecular ingredients, in reponse to an
  appropriate stimulus, which actualizes the
  potential, means that there is a principle of
  intelligibility even if there be no absolute
  reduction.

#### Consistency of Hylemorphism and Modern Science

The idea of a macroscopic substance contradicts
nothing in modern science, for the virtual
existence of each atom or molecule in the
substance would be a reality that justifies
thinking of or modeling the substance in terms of
its ingredients. Any experiment designed to look
for an ingredient atom or molecule could be an
occasion for the probing instrument to actualize
the atom or molecule. Yet a substance, apart from
the material potentialities deriving from its
ingredients, might be expected to have other
potentialities of its own. And these must be
determined by experiment.

Along this line of thought, the idea that the
human individual has a substantial form capable

- not just of preserving material potentialities
  as the individual ingests food

- but also of establishing mental potentialities
  and actualities in the individual

seems perfectly consistent with modern science
(as regards the material), even if it be
inconsistent with scientism (as regards the
mental).

<!-- vim: set tw=49: -->
