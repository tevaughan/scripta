---
layout: post
title: AppImage
image: /assets/img/app-image-cropped.png
tags:
  - app-image
  - inkscape
  - neovim
  - png
  - svg
  - wsl
---

The [AppImage][appimage]-format for distributing
an executable program for a Linux-based system
allows for installation of the program by copying
a single file onto the system, regardless of the
Linux-distribution.

[appimage]: https://appimage.org/

### Even Cooler

As if that were not cool enough, this is
particularly useful in conjunction with
[WSL][wsl] and/or virtual machines.

[wsl]: https://docs.microsoft.com/en-us/windows/wsl/

#### WSL

For example, I have two different
Linux-distributions (Ubuntu '20.04' and
Debian-Buster) installed under WSL.  I wanted to
install '[`neovim-0.5`][neovim-0.5]' onto each
distribution, but neither distribution had that
version of [neovim][neovim] available to the
package-manager. (For Ubuntu '20.04',
'`neovim-0.5`' can in principle be installed via
a ppa, but that would not work for me, perhaps
because of my company's security-system.)

[neovim-0.5]: https://github.com/neovim/neovim/releases/tag/v0.5.0
[neovim]: https://github.com/neovim/neovim

So I downloaded neovim's AppImage-file,
'`nvim.appimage`', into a folder,
```
c:\wsl-appimage
```
on the MS-Windows-filesystem.  Then, inside each
Linux-distribution, I made
```
/usr/local/bin/nvim
```
be a logical link that points to
```
/mnt/c/wsl-appimage/nvim.appimage
```
(This is convenient because I set up WSL so that
the `C`-drive on MS-Windows is always available
under '`/mnt/c`' in the Linux-filesystem.)

Done!

Now typing `nvim` in either distribution runs
neovim, which is installed as a single
AppImage-file in a single location.

#### VM

I did the same thing for an Ubuntu-'20.04' VM
under VirtualBox. After configuring `/mnt/c` for
that VM and setting up the same logical link as
above, it just worked! Instant `nvim`!

### FUSE

In order to point a logical link to an executable
AppImage-file and have it run as a program, one
must have FUSE working in the Linux-distribution.
If the above do not just work, then you might
check out [this page][fuse] to get FUSE set up
properly.

[fuse]: https://docs.appimage.org/user-guide/troubleshooting/fuse.html

Since Debian Buster, one can do
```
sudo apt-get install fuse3
```

### Side-Note

In preparing this post, I wanted to use the
AppImage-project's logo, which comes down in
SVG-format.  I need to convert it to PNG so that
my custom python-script can crop the image.

Normally, I should do that via ImageMagick's
`convert`-utility, but that did not work for this
SVG-file. The color disappeared to black and
white.

So I installed `inkscape` and used the following
command:
```
inkscape -w 256 app-image.svg -o app-image.png
```
which specified a 256-pixel-wide image (while
preserving aspect-ratio).

<!-- vim: set tw=49: -->
