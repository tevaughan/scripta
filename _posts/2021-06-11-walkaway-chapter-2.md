---
layout: post
title: Walkaway, Ch. 2
date: 2021-06-11 14:16:04 -0600
tags:
  - christianity
  - doctorow
  - fiction
  - sexual-morality
  - walkaway
image: /assets/img/walkaway-cropped.png
---

Having just finished Chapter 2 of *Walkaway* (2017) by Cory Doctorow, I have
lowered my opinion from [what it was after Chapter 1][ch1].

[ch1]: {% post_url 2021-06-10-walkaway %}

My purpose in writing about the book is not primarily to review its plot but to
think about the philosophy that Doctorow seems to propose.  That was so in my
[first post][ch1] on the book and is too in the present post.  Yet some context
is needed:

Chapter 2 introduces a woman named Limpopo, who is the primary architect of a
walkaway-tavern called "The Belt and Braces."  The walkaways are those who have
walked away from the world's dominant society, whose economy is controlled by
oligarchs via technology and propaganda.  The walkaways have managed in rural
areas, by way of scavenged technology and raw materials, to provide for
themselves a bounty of material comforts.  This is made possible by the ready
availability of intelligent machines, each of which can be easily powered and
then programmed to fabricate a wide variety of goods, all from easily obtained
feedstock, whether naturally available or scavenged from abandoned junk.
Limpopo is, among the walkaways, one of the great technical architects, and she
is also one of the great exponents of the philosophy and the culture that
originated among the walkaways.  One of the main aspects of the philosophy is
the tension between the commitment to provide plenty of material comfort for
all and the need to be detached from the desire to own material objects.

The main reasons for the lowering of my assessment are
  1. that the portrayal of Christianity is false and
  2. that there is pornography, which glorifies immoral behavior.

Yet despite what is unfortunate, I remain interested in the story.

### False View of Christianity

The first part of Chapter 2 is largely dedicated to Limpopo's teaching Hubert
and his friends the correctness of the philosophy of walkaway.

The culmination of the philosophical introduction is the proposition, that the
philosophy is, according to Limpopo, "Christianity if it had been conceived in
material abundance."  In support of this is Natalie's comment, that the
philosophy of walkaway is "treating everyone like you'd want your family to
treat you."  This is the golden rule.

[gr]: https://en.wikipedia.org/wiki/Golden_Rule

This golden rule, though, is found [(at least according to Wikipedia)][gr] in
most religions and cultures, including Judaism and Christianity.  So it is
*not* what distinguishes Christianity.  Moreover, Christianity has from its
beginning opposed the syncretism that would consider most religions as
essentially equivalent.

[imago]: https://www.firstthings.com/web-exclusives/2015/02/natural-rights-the-imago-dei-and-the-moral-economy-of-sex

Like Judaism before it, Christianity asserts both
- claims (like the golden rule) that are found in many religions and cultures
  across human history and
- claims [(like the Imago Dei)][imago] foundational to civilization yet
  rejected even by the supposedly closely related religion, Islam.

Nothing particularly Christian is to be found in the philosophy of walkaway;
Doctorow presents a false but---perhaps to himself and to many like
him---comfortable view of Christianity.

### Pornography

I should not say much about this.

Right after the presentation of walkaway's philosophy as neo-Christianity
(whereas it is, if anything, pseudo-Christianity), Doctorow proceeds to
describe, tediously and in graphic detail over eight pages, the first sexual
encounter between Hubert and Limpopo.

This is something that nobody should be subjected to, as it glorifies an act
that is (extrinsically) evil.  The description tempts the reader to imagine
what is depicted as a great good to be sought, even when there be no acceptance
of the responsibility toward a child who might naturally be produced.

On the one hand,
- every pleasure is good in itself, and, in particular,
- sexual intercourse is a great good.

On the other hand, what is ignored is that the inappropriate pursuit of a good
can be a grave evil.  In the first chapter, Doctorow acknowledges, through
Hubert, the reality of natural human rights.  Yet here in the treatment of sex
is the disregard for the rights of the human being who would come into
existence if the act luridly described were allowed to have its natural
consequence.

### An Obstacle Presented

The story nevertheless remains interesting, as the walkaways are faced with an
attack at what seems to be their primary weakness, their defenselessness, which
results from their nonviolence and their detachment from material possessions.

