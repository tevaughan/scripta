---
layout: post
title: Science and Truth
date: 2021-06-12 13:27:04 -0600
tags:
  - science
  - truth
image: /assets/img/science-truth-cropped.png
---

What these days is called "science" seems not
itself to be truth, but science does involve
truth of a kind: At least the report of the
sensory experience, which is repeatably
obtained from a well described experiment,
points to an objective truth of nature.

![science-truth][st]

[st]: {{ "/assets/img/science-truth.jpg" | relative_url }}

A friend of mine recently sent me the above
picture with words about science and truth.

While there is a sense in which, as indicated
by the words in the picture, science is a
process of finding truth, this sense seems to
be a weak one. At least in terms of
fundamental physics, "truth" seems not to
apply to the most amazing claims made on the
basis of scientific theory.  Instead, as it
seems to me, the *sensory experience* of a
scientist, on conducting a repeatable
experiment, is a knowable objective truth.
Such truths provide constraints on a
scientific theory.

The theory itself seems often impossible to
know as a truth: A scientific theory can be
ruled out through observation but never ruled
in. The best scientific theory is always only
the best available at present, and there is no
guarantee that insensible things that it
refers to will still be referred to by the
theory that will eventually replace it.

### Repeatable Observation as Truth

Of the two main products of science, only one
of them, **the scientist's description of what
he observed with his senses as the result of
an experiment**, seems for the most part to be
a knowable truth about the object of the
scientist's study.

When the scientist publishes both his
experimental setup and his sensory experience
resulting from the experiment, his peer may
reproduce the result.  If the peer publish a
report of the same experience resulting from
a reproduction of the experiment, then the
community might infer an objective, knowable
truth: that experiment A produces sensory
experience B.

Such a truth might be called a datum. The
scientific community seeks a theory to explain
the available data.

### Theory as Guess that Survives

The other product, **the scientist's theory
explaining a pattern in his observations**,
seems often impossible to be knowable as a
truth.  Worse, history suggests that every
theory depending on postulated but insensible
things is likely to be false; it is only a
matter of time until technology progresses to
the point at which new observations will rule
such a theory out.

Perhaps the occasional theory is knowable as a
truth.  But when the theory refer to something
(like the electron) that can never be sensed
directly by the scientist, then there is
always the possibility that a future theory
might better explain the sensory observations
without mentioning the insensible thing (for
example, a future, superior theory of matter
that makes no mention of the electron).

Our best theory postulating insensible,
physical things seems to be the guess that has
longest (so far) survived the test of
experiment.

